# Tanım: Java Development Kit (JDK) adı verilen bir dizi program.
# URL: http://openjdk.java.net/
# Paketçi: milisarge
# Gerekler: ccache unzip xorg-libxrender xorg-libxtst xorg-libxt zip cups alsa-lib
# Grup: geliştirme

isim=openjdk
surum=8
devir=1
_java_ver=8
_jdk_update=192
_jdk_build=26
pkgver=${_java_ver}.u${_jdk_update}
_repo_ver=jdk${_java_ver}u${_jdk_update}-b${_jdk_build}

kaynak=(http://hg.openjdk.java.net/jdk8u/jdk8u/archive/${_repo_ver}.tar.gz::jdk8u-${_repo_ver}.tar.gz
		http://hg.openjdk.java.net/jdk8u/jdk8u/corba/archive/${_repo_ver}.tar.gz::corba-${_repo_ver}.tar.gz
		http://hg.openjdk.java.net/jdk8u/jdk8u/hotspot/archive/${_repo_ver}.tar.gz::hotspot-${_repo_ver}.tar.gz
		http://hg.openjdk.java.net/jdk8u/jdk8u/jdk/archive/${_repo_ver}.tar.gz::jdk-${_repo_ver}.tar.gz
		http://hg.openjdk.java.net/jdk8u/jdk8u/jaxws/archive/691079f04a6f.tar.gz::jaxws-691079f04a6f.tar.gz
		http://hg.openjdk.java.net/jdk8u/jdk8u/jaxp/archive/${_repo_ver}.tar.gz::jaxp-${_repo_ver}.tar.gz
		http://hg.openjdk.java.net/jdk8u/jdk8u/langtools/archive/${_repo_ver}.tar.gz::langtools-${_repo_ver}.tar.gz
		http://hg.openjdk.java.net/jdk8u/jdk8u/nashorn/archive/${_repo_ver}.tar.gz::nashorn-${_repo_ver}.tar.gz)
	
derle() {
	_JARCH=amd64 
	_jdkname=openjdk8
	_jvmdir=/usr/lib/jvm/java-8-openjdk
	_prefix="jdk8u-${_repo_ver}/image"
	_imgdir="${_prefix}/jvm/openjdk-1.8.0_$(printf '%.2d' ${_jdk_update})"
	_nonheadless=(bin/policytool
	lib/${_JARCH}/libjsound.so
	lib/${_JARCH}/libjsoundalsa.so
	lib/${_JARCH}/libsplashscreen.so)
	cd jdk8u-${_repo_ver}
  
  for subrepo in corba hotspot jdk jaxp langtools nashorn; do
    ln -s ../${subrepo}-${_repo_ver} ${subrepo}
  done
  
  ln -s ../jaxws-691079f04a6f jaxws
  
  unset JAVA_HOME
  # http://icedtea.classpath.org/bugzilla/show_bug.cgi?id=1346
  export MAKEFLAGS=${MAKEFLAGS/-j*}

  # We filter out -O flags so that the optimization of HotSpot is not lowered from O3 to O2
  export CFLAGS="${CFLAGS//-O2/-O3} ${CPPFLAGS} -Wno-error=deprecated-declarations -Wno-error=return-type -Wno-error=cpp -fno-lifetime-dse -fno-delete-null-pointer-checks"
  export CXXFLAGS="${CXXFLAGS} ${CPPFLAGS}"

  install -d -m 755 "${SRC}/${_prefix}/"
  sed -i 's/gcc -V >&5//' configure
  sh configure \
    --prefix="${SRC}/${_prefix}" \
    --with-update-version="${_jdk_update}" \
    --with-build-number="b${_jdk_build}" \
    --with-milestone="fcs" \
    --enable-unlimited-crypto \
    --with-zlib=system \
    --with-extra-cflags="${CFLAGS}" \
    --with-extra-cxxflags="${CXXFLAGS}" \
    --with-extra-ldflags="${LDFLAGS}"

  # TODO OpenJDK does not want last version of giflib (add 'giflib' as dependency once fixed)
  #--with-giflib=system \

  # These help to debug builds: LOG=trace HOTSPOT_BUILD_JOBS=1
  # Without 'DEBUG_BINARIES', i686 won't build: http://mail.openjdk.java.net/pipermail/core-libs-dev/2013-July/019203.html
  make
  make install

  cd ../${_imgdir}

  # http://icedtea.classpath.org/bugzilla/show_bug.cgi?id=1437
  find . -iname '*.jar' -exec chmod ugo+r {} \;
  chmod ugo+r lib/ct.sym

  # remove redundant *diz and *debuginfo files
  find . -iname '*.diz' -exec rm {} \;
  find . -iname '*.debuginfo' -exec rm {} \;

  cd $SRC/${_imgdir}

  # Main files
  install -d -m 755 "${PKG}${_jvmdir}"
  install -d -m 755 "${PKG}${_jvmdir}/jre"

  cp -a include lib "${PKG}${_jvmdir}"
  cp -a jre/bin jre/lib "${PKG}${_jvmdir}/jre"

  # 'bin' files
  pushd bin

  # 'java-rmi.cgi' will be handled separately as it should not be in the PATH and has no man page
  for b in $(ls | grep -v java-rmi.cgi); do
    if [ -e ../jre/bin/${b} ]; then
      # Provide a link of the jre binary in the jdk/bin/ directory
      ln -s ../jre/bin/${b} "${PKG}${_jvmdir}/bin/${b}"
    else
      # Copy binary to jdk/bin/
      install -D -m 755 ${b} "${PKG}${_jvmdir}/bin/${b}"
    fi
  done
  popd

  # Handling 'java-rmi.cgi' separately
  install -D -m 755 bin/java-rmi.cgi "${PKG}${_jvmdir}/bin/java-rmi.cgi"
  
  # Set config files
  mv "${PKG}${_jvmdir}"/jre/lib/management/jmxremote.password{.template,}
  mv "${PKG}${_jvmdir}"/jre/lib/management/snmp.acl{.template,}

  # Link JKS keystore from ca-certificates-utils
  rm -f "${PKG}${_jvmdir}/jre/lib/security/cacerts"
  ln -sf /etc/ssl/certs/java/cacerts "${PKG}${_jvmdir}/jre/lib/security/cacerts"

  # Install license
  install -d -m 755 "${PKG}/usr/share/licenses/java8-openjdk/"
  install -m 644 ASSEMBLY_EXCEPTION LICENSE THIRD_PARTY_README \
                 "${PKG}/usr/share/licenses/java8-openjdk"
  ln -sf /usr/share/licenses/java8-openjdk "${PKG}/usr/share/licenses/${isim}"
}
