# Begin /etc/profile.d/jre.sh

# Set JAVA_HOME directory
JAVA_HOME=/usr/lib/jre

# Adjust PATH
pathappend $JAVA_HOME/bin

AUTO_CLASSPATH_DIR=/usr/lib/java/lib

pathprepend . CLASSPATH

for dir in `find ${AUTO_CLASSPATH_DIR} -type d 2>/dev/null`; do
    pathappend $dir CLASSPATH
done

for jar in `find ${AUTO_CLASSPATH_DIR} -name "*.jar" 2>/dev/null`; do
    pathappend $jar CLASSPATH
done

export JAVA_HOME
unset AUTO_CLASSPATH_DIR dir jar

# End /etc/profile.d/jre.sh
