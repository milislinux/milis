#!/bin/sh
# Bu betik talimatnamede yer alan talimatları web sayfası sunumu için indeksler.
# Kullanım: talimatname_indeks.sh > index.html

paketdepo="http://paketler.milislinux.org"
#paketdepo="http://127.0.0.1:8000"
dosya=/tmp/paket_konumlar
[ -f $dosya ] && rm -rf $dosya
find /root/talimatname/temel/* -type d  > $dosya
#find /root/talimatname/genel/*/* -type d  >> $dosya

echo "<html>"
echo '<meta http-equiv="content-type" contentType="text/html; charset=UTF-8">'
echo '<meta charset="UTF-8">'
echo '<link rel="stylesheet" href="bootstrap.min.css" />'
echo '<table class="table" border=1>'
echo '<thead class="thead-dark">'
echo '<tr>'
echo '<th scope="col">Grup</th>'
echo '<th scope="col">isim</th>'
echo '<th scope="col">Sürüm</th>'
echo '<th scope="col">Açıklama</th>'
echo '<th scope="col">Url</th>'
echo '<th scope="col">Paket Adresi</th>'
echo '<th scope="col">Son Güncelleme</th>'
echo '</tr>'
echo '</thead>'

echo '<tbody>'
	
while IFS='' read -r konum || [[ -n "$konum" ]]; do
	
	echo "<tr>"
	
	# Grup
	grup=$(grep -ri "# Grup:" $konum/talimat | cut -d ':' -f2-)
	echo "<td>$grup</td>"
	
	# İsim
	isim=$(basename $konum)
	echo "<td>$isim</td>"
	
	# Surum-Devir
	surum=$(grep -ri ^"surum=" $konum/talimat | cut -d '=' -f2-)
	devir=$(grep -ri ^"devir=" $konum/talimat | cut -d '=' -f2-)
	echo "<td>$surum-$devir</td>"
	
	# Tanım
	tanim=$(grep -ri "# Tanım:" $konum/talimat | cut -d ':' -f2-)
	echo "<td>$tanim</td>"
	
	# Url
	url=$(grep -ri "# Url:" $konum/talimat | cut -d ':' -f2-)
	echo "<td><a href=$url>$url</a></td>"
	
	# İndir
	purl="${paketdepo}/${isim}%23${surum}-x86_64.mps.lz"
	echo "<td><a href=${purl}>indir</a></td>"
	# %23 = #
	
	# Güncelleme Tarih
	gtarih=$(curl -s -v -X HEAD $purl 2>&1 | grep '^< Last-Modified:' | cut -d':' -f2-)
    echo "<td>$gtarih</td>"
	
	echo "</tr>"
done < "$dosya"

echo '</tbody>'
echo "</table>"
echo "</htlm>"

