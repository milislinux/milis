#!/bin/bash
echo "**** Milis Linux Sabit Derleme Alanını Oluşturup Gitmenizi Sağlar. ****"
echo "**** İşlem sonrası mps -GG ve mps -G komutlarını veriniz. ****"

# derleme ortamının indirilmesi

ortam_sha="55ac07a07eee49ba24ac8cee5db9cad2da441a55a63fa445450a4977fe7aca53" 
ortam_adres="http://kaynaklar.milislinux.org/iso/milis-bootstrap-enson.sfs"
ortam=/mnt/milis-bootstrap-enson.sfs

shasum_kontrol(){
	inen_sha=$(sha256sum $ortam | cut -d' ' -f1)
	if [ "$ortam_sha" = "$inen_sha" ];then
		echo "ortam shasum doğrulandı."
	else
		echo "indirilen ortamda shasum uyşmazlığı var.silip tekrar deneyiniz.";exit 1
	fi
}	

if [ ! -f $ortam ];then
	wget $ortam_adres -O $ortam
	shasum_kontrol
fi

cd /mnt
unsquashfs milis-bootstrap-enson.sfs
mv squashfs-root lfs-sabit

cd /sources/milis.git
	echo "sources/milis.git yapıldı"
export LFS=/mnt/lfs-sabit
	echo "LFS=/mnt/lfs-sabit yapıldı"
./lfs-mekanizma  -cg
